package inventory;

public class Inventory {
    private final Item[][] items;

    public Inventory(int height, int width) {
        items = new Item[height][width];
    }

    public boolean store(Item item, int row, int column) {
        if (row < 0 || row >= items.length || column < 0 || column >= items[0].length) {
            System.out.println("Location " + row + ", " + column + " is out of bounds");
            return false;
        }
        if (items[row][column] != null) {
            System.out.println("There's already a " + items[row][column].getName() + " here");
            return false;
        }
        items[row][column] = item;
        return true;
    }

    public Item retrieve(int row, int column) {
        if (row < 0 || row >= items.length || column < 0 || column >= items[0].length) {
            System.out.println("Location " + row + ", " + column + " is out of bounds");
            return null;
        }
        if (items[row][column] == null) {
            System.out.println("There's nothing here");
            return null;
        }
        return items[row][column];
    }
}
