package inventory;

import java.util.EnumMap;
import java.util.Map;

public class Item {

    private final String name;
    private final ItemType itemType;
    private final Map<PropertyType, Integer> properties = new EnumMap<>(PropertyType.class);

    public Item(String name, ItemType weapon) {
        this.name = name;
        this.itemType = weapon;
    }

    public void setProperty(PropertyType property, int value) {
        this.properties.put(property, value);
    }

    public int getProperty(PropertyType property) {
        return this.properties.getOrDefault(property, 0);
    }

    public String getName() {
        return this.name;
    }
}
