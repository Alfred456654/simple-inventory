package inventory;

public enum ItemType {
    WEAPON,
    ARMOR,
    CONSUMABLE;
}
