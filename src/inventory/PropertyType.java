package inventory;

public enum PropertyType {
    DAMAGE,
    PRICE,
    RARITY;
}
